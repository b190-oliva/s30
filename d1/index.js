// Section - MongoDB Aggregation
/*
	- used to generate manipulated data and perform operations to createa filtered results
	- compared to CRUD operations on our data, aggregation gives us access to manipulate, filter and compute for results.
*/

//Section - using aggregate method
/*
	$match
	- used to pass the documents that meet the specified coniditions(s), to the next pipeline/aggregation process
*/
db.fruits.aggregate([
	{$match: {onSale: true}}

]);

// $group
/*
	- grouping certain field and values $sum used to add all of the values
*/

db.fruits.aggregate([
	{$group: {_id:"$supplier_id", total: {$sum: "$stock"}}}
]);

// using $match ng $group along w/ aggregation will find for the products that are on sale and will group the total stock.
// pipeline / aggregation process is the series of aggregation methods in mongoDB

// ===pipelines===

db.fruits.aggregate([
        {$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

// Field projections with aggregation

db.fruits.aggregate([
        {$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id:0}}
]);

// $sort - used to change the order of aggregated results
// providing -1 as the value will result to reverse order
// providing 1 value on the other hand will sort it ascending order

db.fruits.aggregate([
        {$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{sort: {total:-1}}
]);

// kinds

// db.fruits.aggregate([
// 	{$group:{_id: "$origin", kinds: {$sum:1}}}
// ]);

// unwind - used to deconstruct an array field from a collection/ field with array value to output a result for each element.

db.fruits.aggregate([
	{$unwind: "$origin"},
]);

// unwind then group
/*
	code below won't work if the goal is to group the documents based on 
*/

// code below will deconstruct the first documents based on origin field and group them based on the number of times a country is mentioned on the origin field

db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group:{_id: "$origin", kinds: {$sum:1}}}
]);

// Section - schema design
/*
	- data modelling, normalized or de-normalized/embedded data
	- normalized data refers to a data structure where documents are reffered to each other using their ids for related oieces of information
	- de-normalized data / embedded data design refers to a data structure where related pieces of information are added to a document as an embedded object
*/


// normalized data schema design in one to one relationship

var owner = ObjectID();

db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "0912345678"

});

// mini activity - inserting another collection but with the same ID by owners collection John
var suppliers = ObjectId();

db.suppliers.insert({
	name: "GMA7",
	contact: "09123456789",
	_id: ObjectId("62d5476ed5ab2d97b5002d9d")
});

//de normalized data schema design with one is to two relationship

db.suppliers.insert({
	name: "DEF fruits",
	contact: "09123456789",
	address: [
		{street: "123 San Jose St.", city: "Manila"},
		{street: "367 Gil Puyat", city: "Makati"}
	]
});

// mini-activity

var supplier = ObjectId();
var branch = ObjectId();

db.suppliers.insert({
	_id: supplier,
	name: "ABSCBN",
	contact: "091234568374",
	branches: [branch]
});

db.branch.insert({
	_id: ObjectId("62d5529dd5ab2d97b5002da5"),
	name: "fruity pops",
	address: "10th ave",
	city: "Cubao, Metro Manila",
	supplier_id: ObjectId("62d5529dd5ab2d97b5002da4")
});



