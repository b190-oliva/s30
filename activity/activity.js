/*
	5. Use the max operator to get the highest price of a fruit per supplier.
	6. Use the min operator to get the lowest price of a fruit per supplier.
*/

// total number of fruits on sale
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "fruitsOnSale"}
]);

// total number of fruits with stock more than 20

db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}},
    {$count: "enoughStock"}
    
]);

// average price of fruits that is onSale per supplier

db.fruits.aggregate([
    {$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);

// highest price of a fruit per supplier

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
    {$sort: {max_price: 1}}
]);

//lowest price of fruit per supplier

db.fruits.aggregate([
    {$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
	{$sort: {min_price: 1}}
]);
